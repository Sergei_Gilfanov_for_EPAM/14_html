#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# enable debugging
import time
import codecs, sys
import cgi
import cgitb
import json

#cgitb.enable(display=0, logdir="/tmp/pycgi/debug")
cgitb.enable()

sys.stdout = codecs.getwriter("utf-8")(sys.stdout.detach())
form = cgi.FieldStorage()

#История заказов. Верхний ключ - id заказа
history = {
    '1': [
            { 
                '_type': 'statusChange',
                'status': "new",
                'comment': "Комментарий 1"
            },
            { 
                '_type': 'statusChange',
                'status': "waiting",
                'comment': "Комментарий 2"
            },
            { 
                '_type': 'comment',
                'comment': "Комментарий 3"
            },
            { 
                '_type': 'statusChange',
                'status': "done",
                'comment': "Комментарий 4"
            },
    ],
    '2': [
            { 
                '_type': 'statusChange',
                'status': "new",
                'comment': "Заказ создан"
            },
            { 
                '_type': 'statusChange',
                'status': "waiting",
                'comment': "Комментарий 2"
            },
            { 
                '_type': 'comment',
                'comment': "Комментарий 3"
            }
    ],
    '3': [
            { 
                '_type': 'statusChange',
                'status': "new",
                'comment': "Новый заказ"
            },
            { 
                '_type': 'statusChange',
                'status': "waiting",
                'comment': "Комментарий 2"
            }
    ],
    '4': [
            { 
                '_type': 'statusChange',
                'status': "new",
                'comment': "Новый заказ созда"
            }
    ],

    '5': [ # Будет возращаться createOrder
            {
                '_type': 'statusChange',
                'status': "new",
                'comment': "Новый заказ созда"
            }
    ]
}


order_id = form.getvalue('orderId');
retval = history[order_id]

print("Content-Type: application/json; charset=utf-8")
print()


print(json.dumps(retval, ensure_ascii=False))
