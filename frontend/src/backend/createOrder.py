#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# enable debugging
import time
import codecs, sys
import cgi
import cgitb
import json

#cgitb.enable(display=0, logdir="/tmp/pycgi/debug")
cgitb.enable()

sys.stdout = codecs.getwriter("utf-8")(sys.stdout.detach())
form = cgi.FieldStorage()

client_id = form.getvalue('clientId');
retval = { 'id': 4 }

print("Content-Type: application/json; charset=utf-8")
print()


print(json.dumps(retval, ensure_ascii=False))
