"use strict";
$(document).ready( function(window) {

	var pages;
	var popups;

	var dao = (function() {
		var backendBase = "/backend";
		var clientList = (function() {
			var clientListCache = null;
			function get(refresh) {
				var getDone = $.Deferred();

				if ( !refresh && list != null ) {
					getDone.resolve(clientListCache);
					return getDone;
				}

				$.get( backendBase + "/clientList.py", {} )
				.done(function(data) {
					clientListCache = data;
					getDone.resolve(data);
				})
				.fail(function() {
					getDone.reject();
				});
				return getDone;
			};

			return {
				get: get
			}
		})();

		var client = (function() {
			function create(serializedForm) {
				return $.post( backendBase + "/createClient.py", serializedForm);
			}

			return {
				create: create
			}
		})();

		var orderList = (function() {
			var orderListCache = null;
			function get(client, refresh) {
				var getDone = $.Deferred();

				if (!refresh && orderListCache != null ) {
					getDone.resolve(orderListCache);
					return getDone;
				}

				$.get( backendBase + "/getOrders.py", {clientId: client.id} )
				.done(function(data) {
					orderListCache = data;
					getDone.resolve(data);
				})
				.fail(function() {
					getDone.reject();
				});
				return getDone;
			};

			return {
				get: get
			}
		})();

		var order = (function() {

			function create(client) {
				return $.post( backendBase + "/createOrder.py", {clientId:client.id});
			}

			function get(order) {
				return $.post(backendBase + "/getOrder.py", {orderId:order.id});
			}

			function addMessage(order, messageText) {
				return $.post( backendBase + "/createComment.py", {orderId:order.id, messageText:messageText});
			}

			return {
				create: create,
				get: get,
				addMessage: addMessage
			}
		})();

		var orderHistory = (function() {
			var orderHistoryCache = null;

			function get(order, refresh) {
				var getDone = $.Deferred();

				if ( !refresh && orderHistoryCache != null ) {
					getDone.resolve(orderHistoryCache);
					return getDone;
				}

				$.get( backendBase + "/getOrderHistory.py", {orderId: order.id} )
				.done(function(data) {
					orderHistoryCache = data;
					getDone.resolve(data);
				})
				.fail(function() {
					getDone.reject();
				});
				return getDone;
			};

			function addComment(historyItem) {
				var retval = $.post( backendBase + "/createComment.py", { orderId: order.id, comment: historyItem.comment });
				retval.done( function(data) {
					orderHistoryCache.push(historyItem);
				});
				return retval;
			};

			function changeStatus(historyItem) {
				var retval = $.post ( backendBase + "/changeStatus.py", { orderId: order.id, 'status': historyItem.status, comment: historyItem.comment } );
				retval.done( function(data) {
					 orderHistoryCache.push(historyItem);
				});
				return retval;
			}

			function getCurrentStatus() {
				var retval = null;
				$.each(orderHistoryCache, function(i, historyItem) {
					if ( historyItem._type == 'statusChange' ) {
						retval = historyItem.status;
					}
				});
				return retval;
			}

			return {
				get: get,
				addComment: addComment,
				changeStatus: changeStatus,
				getCurrentStatus: getCurrentStatus
			};

		})();

		return {
			clientList: clientList,
			client: client,
			orderList: orderList,
			order: order,
			orderHistory: orderHistory
		}
	})();

	pages = (function(){
		var clientList = (function() {
			var clientListPageElement = $('.client-list-page');
			var clientListElement = $('.client-list')
			var clientListItemTemplate = clientListElement.find('.li-template').detach();
			clientListItemTemplate.removeClass("li-template");
			var clientListLoadingElement = clientListElement.find('.li-loading');

			clientListElement.on( 'click', '.client-list-item', function() {
				var clientData = $(this).data().clientData;
				hide();
				pages.client.show( clientData);
			});

			clientListPageElement.find('.add-client-button').click(function() {
				popups.clientAdd.show()
				.done(function(newClient) {
					var clientElement = createClientElement(newClient);
					clientListElement.append(clientElement);
				});
			});

			function show(clientListDao) {
				clientListLoadingElement.show();
				clientListPageElement.show();

				var showDone = $.Deferred();
				clientListDao.get(true)
				.done(function(clientList) {
					clientListLoadingElement.hide();
					var toAppend = [];
					$.each(clientList, function(i, client) {
						var clientElement = createClientElement(client);
						toAppend.push(clientElement);
					});
					clientListElement.append(toAppend);
					showDone.resolve();
				});
				return showDone;
			}

			function hide() {
				clientListPageElement.hide();
				clientListElement.find('.client-list-item').remove();
			}

			function createClientElement(client) {
				var clientElement = clientListItemTemplate.clone();
				clientElement.find('.client-name').text(client.clientName);
				clientElement.data('clientData', client);
				return clientElement;
			}

			return {
				'show': show
			}
		})();

		var client = (function() {
			var clientPageElement = $('.client-page');
			var orderListElement = $('.order-list');
			var orderListItemTemplate = orderListElement.find('.li-template').detach();
			orderListItemTemplate.removeClass('li-template');
			var orderListLoadingElement = orderListElement.find('.li-loading');

			orderListElement.on( 'click', '.order-list-item', function() {
				var orderData = $(this).data().orderData;
				var orderElement = $(this);
				popups.orderHistory.show(orderData)
				.done( function (newStatus) {
					orderElement.data().orderData.status = newStatus;
					orderElement.find('.order-status').text(newStatus);
				});
			});

			clientPageElement.find('.add-order-button').click(function() {
				popups.orderAdd.show(client)
				.done(function(newOrder) {
					var orderElement = createOrderElement(newOrder);
					orderListElement.append(orderElement);
				});
			});

			function show(aClient) {
				client = aClient;
				orderListLoadingElement.show();
				clientPageElement.show();

				var showDone = $.Deferred();
				dao.orderList.get(aClient, true)
				.done(function(orderList) {
					orderListLoadingElement.hide();
					var toAppend = [];
					$.each(orderList, function(i, order) {
						var orderElement = createOrderElement(order);
						toAppend.push(orderElement);
					});
					orderListElement.append(toAppend);
					showDone.resolve();
				});
				return showDone;
			}

			function hide() {
				clientPageElement.hide();
				orderListElement.find('.order-list-item').remove();
			}

			function createOrderElement(order) {
				var orderElement = orderListItemTemplate.clone();
				orderElement.find('.order-number').text(order.id);
				orderElement.find('.order-status').text(order.status);
				orderElement.data('orderData', order);
				return orderElement;
			}

			return {
				'show': show
			}
		})();

		return {
			clientList: clientList,
			client: client
		}
	})();

	popups = (function() {
		var overlayElement = $('.popup-overlay');

		var clientAdd = (function() {
			var inputDone = null;

			var popupElement = $('.client-add');
			var formElement = popupElement.find('form');

			popupElement.on( 'click', '.close-button', function() {
				hide();
				inputDone.reject();
			});

			formElement.find('.button').click( function(event) {
				event.preventDefault();
				dao.client.create(formElement.serialize())
				.done(function(data) {
					hide();
					console.log(data);
					inputDone.resolve(data);
				});
			});

			function show() {
				inputDone = $.Deferred();
				popupElement.show();
				overlayElement.show();
				return inputDone;
			}

			function hide() {
				overlayElement.hide();
				popupElement.hide();
			}

			return {
				'show': show
			}
		})();

		var orderAdd = (function() {
			var client;
			var inputDone = null;

			var popupElement = $('.order-add');
			var formElement = popupElement.find('form');
			var commentElement = formElement.find('textarea[name="comment"]');

			popupElement.on( 'click', '.close-button', function() {
				hide();
				inputDone.reject();
			});

			formElement.find('.button').click( function(event) {
				event.preventDefault();
				var orderCreated = dao.order.create(client);

				var messageAdded = orderCreated.then( function(order) {
					return dao.order.addMessage(order, commentElement.val());
				});

				var statusRead = orderCreated.then(dao.order.get);

				$.when(statusRead, messageAdded)
				.then(function(orderAjaxRes, messageAjaxRes) {
					hide();
					inputDone.resolve(orderAjaxRes[0]);
				});
			});

			function show(aClient) {
				client = aClient;
				inputDone = $.Deferred();
				popupElement.show();
				overlayElement.show();
				return inputDone;
			}

			function hide() {
				overlayElement.hide();
				popupElement.hide();
			}

			return {
				'show': show
			}
		})();

		var orderHistory = (function() {
			var inputDone = null;
			var popupElement = $('.order-history-popup');
			var orderHistoryElement = $('.order-history');
			var historyItemTemplateComment = orderHistoryElement.find('.li-template.order-history-item-comment').detach();
			historyItemTemplateComment.removeClass('li-template');
			var historyItemTemplateStatusChange = orderHistoryElement.find('.li-template.order-history-item-status-change').detach();
			historyItemTemplateStatusChange.removeClass('li-template');

			var formElement = popupElement.find('form');
			var statusInput = formElement.find('select[name=order-status]');
			var commentInput = formElement.find('textarea[name=comment]');

			var orderHistoryLoadigElement = orderHistoryElement.find('.li-loading');

			var order = null;
			var orderChanged = false;

			function changeStatus(historyItem) {
				return $.post ( backendBase + "/changeStatus.py", { orderId: order.id, 'status': historyItem.status, comment: historyItem.comment } );
			}

			function cleanInputs() {
				commentInput.val("");
				statusInput.val("");
			}

			popupElement.on( 'click', '.close-button', function() {
				hide();
				if (!orderChanged) {
					inputDone.reject();
				} else {
					inputDone.resolve(dao.orderHistory.getCurrentStatus());
				}
			});


			formElement.find('.button').click( function(event) {
				event.preventDefault();
				var historyItem = null;
				var commandFn = null;
				switch( statusInput.val() ) {
					case '':
						historyItem = { '_type': "comment", comment: commentInput.val() };
						commandFn = dao.orderHistory.addComment;
						break;
					default:
						historyItem = { '_type': "statusChange", 'status':  statusInput.val(), comment:  commentInput.val() };
						commandFn = dao.orderHistory.changeStatus;
						break;
				}

				commandFn(historyItem).done( function(data) {
					orderChanged = true;
					var historyItemElement = createHistoryItemElement(historyItem);
					orderHistoryElement.append(historyItemElement);
					cleanInputs();
				});
			});

			function show(orderData) {
				order = orderData;
				inputDone = $.Deferred();
				orderHistoryLoadigElement.show();
				popupElement.show();
				overlayElement.show();

				dao.orderHistory.get(orderData, true)
				.done(function(orderHistory) {
					orderHistoryLoadigElement.hide();
					var toAppend = [];
					$.each(orderHistory, function(i, historyItem) {
						var historyItemElement = createHistoryItemElement(historyItem);
						toAppend.push(historyItemElement);
					});
					orderHistoryElement.append(toAppend);
				});
				return inputDone;
			}

			function hide() {
				overlayElement.hide();
				popupElement.hide();
				orderHistoryElement.find('.order-history-item').remove();
			}

			function createHistoryItemElement(historyItem) {
				var retval = null;
				switch (historyItem._type) {
					case 'statusChange':
						retval = historyItemTemplateStatusChange.clone();
						retval.find('.history-item-status').text(historyItem.status);
						retval.find('.history-item-comment').text(historyItem.comment);
						break;
					case 'comment':
						retval = historyItemTemplateComment.clone();
						retval.find('.history-item-comment').text(historyItem.comment);
						break;
				}
				return retval;
			}

			return {
				'show': show
			}
		})();

		return {
			clientAdd: clientAdd,
			orderAdd: orderAdd,
			orderHistory: orderHistory
		};
	})();

	pages.clientList.show(dao.clientList);
});
